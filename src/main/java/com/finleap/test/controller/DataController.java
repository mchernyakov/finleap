package com.finleap.test.controller;

import com.finleap.test.model.Data;
import com.finleap.test.service.DataService;
import com.finleap.test.util.CharUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class DataController {

    private final DataService dataService;

    @Autowired
    public DataController(DataService dataService) {
        this.dataService = dataService;
    }

    @GetMapping("/data")
    public Data getWeatherDataByCity(@Valid @RequestParam(value = "city") String city) {
        if (CharUtil.checkLettersInString(city)) {
            return dataService.getWeatherDataByCity(city);
        } else throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Wrong city name");
    }

}
