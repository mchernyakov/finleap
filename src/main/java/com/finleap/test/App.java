package com.finleap.test;

import com.typesafe.config.Config;
import com.finleap.test.config.MainConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;

import java.util.List;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        String[] profiles = getProfiles();
        new SpringApplicationBuilder()
                .profiles(profiles)
                .sources(App.class)
                .run(args);
    }

    private static String[] getProfiles() {
        Config config = MainConfig.springConfig();
        List<String> profiles = config.getStringList("profiles");
        return profiles.toArray(new String[0]);
    }
}
