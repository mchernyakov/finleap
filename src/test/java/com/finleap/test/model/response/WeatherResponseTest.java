package com.finleap.test.model.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

public class WeatherResponseTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static final String W_R_DATA = "{\n" +
            "  \"cod\": \"200\",\n" +
            "  \"message\": 0.0082,\n" +
            "  \"cnt\": 32,\n" +
            "  \"list\": [\n" +
            "    {\n" +
            "      \"dt\": 1552078800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 2.77,\n" +
            "        \"temp_min\": 2.77,\n" +
            "        \"temp_max\": 2.97,\n" +
            "        \"pressure\": 1004.26,\n" +
            "        \"sea_level\": 1004.26,\n" +
            "        \"grnd_level\": 984.44,\n" +
            "        \"humidity\": 93,\n" +
            "        \"temp_kf\": -0.2\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 44\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.06,\n" +
            "        \"deg\": 224.502\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.0175\n" +
            "      },\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-08 21:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552089600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 2.82,\n" +
            "        \"temp_min\": 2.82,\n" +
            "        \"temp_max\": 2.98,\n" +
            "        \"pressure\": 1003.5,\n" +
            "        \"sea_level\": 1003.5,\n" +
            "        \"grnd_level\": 983.65,\n" +
            "        \"humidity\": 93,\n" +
            "        \"temp_kf\": -0.15\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 44\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.22,\n" +
            "        \"deg\": 225.5\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.84\n" +
            "      },\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 00:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552100400,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 2.11,\n" +
            "        \"temp_min\": 2.11,\n" +
            "        \"temp_max\": 2.22,\n" +
            "        \"pressure\": 1002.77,\n" +
            "        \"sea_level\": 1002.77,\n" +
            "        \"grnd_level\": 982.88,\n" +
            "        \"humidity\": 94,\n" +
            "        \"temp_kf\": -0.1\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 24\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.31,\n" +
            "        \"deg\": 229.001\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.01\n" +
            "      },\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 03:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552111200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 2.08,\n" +
            "        \"temp_min\": 2.08,\n" +
            "        \"temp_max\": 2.13,\n" +
            "        \"pressure\": 1003.18,\n" +
            "        \"sea_level\": 1003.18,\n" +
            "        \"grnd_level\": 983.33,\n" +
            "        \"humidity\": 95,\n" +
            "        \"temp_kf\": -0.05\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 88\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 6.17,\n" +
            "        \"deg\": 242.502\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.065\n" +
            "      },\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 06:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552122000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 1.29,\n" +
            "        \"temp_min\": 1.29,\n" +
            "        \"temp_max\": 1.29,\n" +
            "        \"pressure\": 1004.59,\n" +
            "        \"sea_level\": 1004.59,\n" +
            "        \"grnd_level\": 984.8,\n" +
            "        \"humidity\": 91,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 801,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"few clouds\",\n" +
            "          \"icon\": \"02d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 20\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 7.38,\n" +
            "        \"deg\": 253\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 09:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552132800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 1.23,\n" +
            "        \"temp_min\": 1.23,\n" +
            "        \"temp_max\": 1.23,\n" +
            "        \"pressure\": 1005.02,\n" +
            "        \"sea_level\": 1005.02,\n" +
            "        \"grnd_level\": 985.05,\n" +
            "        \"humidity\": 90,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 68\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 7.96,\n" +
            "        \"deg\": 248.502\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.0020000000000002\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 12:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552143600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.86,\n" +
            "        \"temp_min\": 0.86,\n" +
            "        \"temp_max\": 0.86,\n" +
            "        \"pressure\": 1006.25,\n" +
            "        \"sea_level\": 1006.25,\n" +
            "        \"grnd_level\": 986.32,\n" +
            "        \"humidity\": 87,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 801,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"few clouds\",\n" +
            "          \"icon\": \"02d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 24\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 7.11,\n" +
            "        \"deg\": 257.003\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 15:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552154400,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.54,\n" +
            "        \"temp_min\": 0.54,\n" +
            "        \"temp_max\": 0.54,\n" +
            "        \"pressure\": 1007.52,\n" +
            "        \"sea_level\": 1007.52,\n" +
            "        \"grnd_level\": 987.47,\n" +
            "        \"humidity\": 87,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 12\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 6.01,\n" +
            "        \"deg\": 254.502\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.0049999999999999\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 18:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552165200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -0.35,\n" +
            "        \"temp_min\": -0.35,\n" +
            "        \"temp_max\": -0.35,\n" +
            "        \"pressure\": 1007.65,\n" +
            "        \"sea_level\": 1007.65,\n" +
            "        \"grnd_level\": 987.5,\n" +
            "        \"humidity\": 87,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"02n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 8\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.51,\n" +
            "        \"deg\": 237.5\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-09 21:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552176000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -1.25,\n" +
            "        \"temp_min\": -1.25,\n" +
            "        \"temp_max\": -1.25,\n" +
            "        \"pressure\": 1006.09,\n" +
            "        \"sea_level\": 1006.09,\n" +
            "        \"grnd_level\": 985.98,\n" +
            "        \"humidity\": 88,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 92\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 3.91,\n" +
            "        \"deg\": 199.504\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.05\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 00:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552186800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -1.57,\n" +
            "        \"temp_min\": -1.57,\n" +
            "        \"temp_max\": -1.57,\n" +
            "        \"pressure\": 1003.82,\n" +
            "        \"sea_level\": 1003.82,\n" +
            "        \"grnd_level\": 983.71,\n" +
            "        \"humidity\": 89,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 44\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.45,\n" +
            "        \"deg\": 197.502\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.02\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 03:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552197600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -0.15,\n" +
            "        \"temp_min\": -0.15,\n" +
            "        \"temp_max\": -0.15,\n" +
            "        \"pressure\": 1001.9,\n" +
            "        \"sea_level\": 1001.9,\n" +
            "        \"grnd_level\": 981.92,\n" +
            "        \"humidity\": 91,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 44\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.09,\n" +
            "        \"deg\": 199.503\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.02\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 06:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552208400,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 1.63,\n" +
            "        \"temp_min\": 1.63,\n" +
            "        \"temp_max\": 1.63,\n" +
            "        \"pressure\": 1000.04,\n" +
            "        \"sea_level\": 1000.04,\n" +
            "        \"grnd_level\": 980.32,\n" +
            "        \"humidity\": 96,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 88\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.96,\n" +
            "        \"deg\": 196.51\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.05\n" +
            "      },\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.04\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 09:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552219200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 1.59,\n" +
            "        \"temp_min\": 1.59,\n" +
            "        \"temp_max\": 1.59,\n" +
            "        \"pressure\": 997.96,\n" +
            "        \"sea_level\": 997.96,\n" +
            "        \"grnd_level\": 978.24,\n" +
            "        \"humidity\": 96,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 92\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.38,\n" +
            "        \"deg\": 185.001\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.275\n" +
            "      },\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.0024999999999999\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 12:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552230000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.96,\n" +
            "        \"temp_min\": 0.96,\n" +
            "        \"temp_max\": 0.96,\n" +
            "        \"pressure\": 995.81,\n" +
            "        \"sea_level\": 995.81,\n" +
            "        \"grnd_level\": 976.04,\n" +
            "        \"humidity\": 95,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 88\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 3.96,\n" +
            "        \"deg\": 182\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.24\n" +
            "      },\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.225\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 15:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552240800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.55,\n" +
            "        \"temp_min\": 0.55,\n" +
            "        \"temp_max\": 0.55,\n" +
            "        \"pressure\": 994.33,\n" +
            "        \"sea_level\": 994.33,\n" +
            "        \"grnd_level\": 974.5,\n" +
            "        \"humidity\": 95,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 88\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 3.42,\n" +
            "        \"deg\": 205.504\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.015\n" +
            "      },\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 1.9075\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 18:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552251600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.68,\n" +
            "        \"temp_min\": 0.68,\n" +
            "        \"temp_max\": 0.68,\n" +
            "        \"pressure\": 993.04,\n" +
            "        \"sea_level\": 993.04,\n" +
            "        \"grnd_level\": 973.2,\n" +
            "        \"humidity\": 97,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 92\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 2.65,\n" +
            "        \"deg\": 227.006\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.035\n" +
            "      },\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.1925\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-10 21:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552262400,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.43,\n" +
            "        \"temp_min\": 0.43,\n" +
            "        \"temp_max\": 0.43,\n" +
            "        \"pressure\": 992.99,\n" +
            "        \"sea_level\": 992.99,\n" +
            "        \"grnd_level\": 973.13,\n" +
            "        \"humidity\": 96,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 88\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 3.62,\n" +
            "        \"deg\": 252\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 1.03\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 00:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552273200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.5,\n" +
            "        \"temp_min\": 0.5,\n" +
            "        \"temp_max\": 0.5,\n" +
            "        \"pressure\": 994.05,\n" +
            "        \"sea_level\": 994.05,\n" +
            "        \"grnd_level\": 974.04,\n" +
            "        \"humidity\": 96,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 88\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.47,\n" +
            "        \"deg\": 251.001\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.245\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 03:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552284000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -0.39,\n" +
            "        \"temp_min\": -0.39,\n" +
            "        \"temp_max\": -0.39,\n" +
            "        \"pressure\": 995.63,\n" +
            "        \"sea_level\": 995.63,\n" +
            "        \"grnd_level\": 975.68,\n" +
            "        \"humidity\": 92,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.11,\n" +
            "        \"deg\": 260.503\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.0125\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 06:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552294800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.34,\n" +
            "        \"temp_min\": 0.34,\n" +
            "        \"temp_max\": 0.34,\n" +
            "        \"pressure\": 995.68,\n" +
            "        \"sea_level\": 995.68,\n" +
            "        \"grnd_level\": 975.87,\n" +
            "        \"humidity\": 95,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 801,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"few clouds\",\n" +
            "          \"icon\": \"02d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 20\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 2.97,\n" +
            "        \"deg\": 240.002\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 09:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552305600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.62,\n" +
            "        \"temp_min\": 0.62,\n" +
            "        \"temp_max\": 0.62,\n" +
            "        \"pressure\": 992.51,\n" +
            "        \"sea_level\": 992.51,\n" +
            "        \"grnd_level\": 972.78,\n" +
            "        \"humidity\": 95,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 88\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 2.62,\n" +
            "        \"deg\": 128.002\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.0725\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 12:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552316400,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.23,\n" +
            "        \"temp_min\": 0.23,\n" +
            "        \"temp_max\": 0.23,\n" +
            "        \"pressure\": 987.97,\n" +
            "        \"sea_level\": 987.97,\n" +
            "        \"grnd_level\": 968.16,\n" +
            "        \"humidity\": 95,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 601,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"snow\",\n" +
            "          \"icon\": \"13d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 92\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.55,\n" +
            "        \"deg\": 96.501\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 4\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 15:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552327200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 0.08,\n" +
            "        \"temp_min\": 0.08,\n" +
            "        \"temp_max\": 0.08,\n" +
            "        \"pressure\": 985.97,\n" +
            "        \"sea_level\": 985.97,\n" +
            "        \"grnd_level\": 965.84,\n" +
            "        \"humidity\": 95,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 601,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"snow\",\n" +
            "          \"icon\": \"13n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 92\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 3.33,\n" +
            "        \"deg\": 37.5055\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 4.65\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 18:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552338000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -0.07,\n" +
            "        \"temp_min\": -0.07,\n" +
            "        \"temp_max\": -0.07,\n" +
            "        \"pressure\": 990.94,\n" +
            "        \"sea_level\": 990.94,\n" +
            "        \"grnd_level\": 970.98,\n" +
            "        \"humidity\": 94,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 76\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.56,\n" +
            "        \"deg\": 300.502\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.435\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-11 21:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552348800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -2.91,\n" +
            "        \"temp_min\": -2.91,\n" +
            "        \"temp_max\": -2.91,\n" +
            "        \"pressure\": 995.38,\n" +
            "        \"sea_level\": 995.38,\n" +
            "        \"grnd_level\": 975.25,\n" +
            "        \"humidity\": 92,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 6.05,\n" +
            "        \"deg\": 283.001\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.3125\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-12 00:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552359600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -5.23,\n" +
            "        \"temp_min\": -5.23,\n" +
            "        \"temp_max\": -5.23,\n" +
            "        \"pressure\": 998.21,\n" +
            "        \"sea_level\": 998.21,\n" +
            "        \"grnd_level\": 977.97,\n" +
            "        \"humidity\": 87,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 802,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"scattered clouds\",\n" +
            "          \"icon\": \"03n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 44\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.82,\n" +
            "        \"deg\": 264\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-12 03:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552370400,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -5.41,\n" +
            "        \"temp_min\": -5.41,\n" +
            "        \"temp_max\": -5.41,\n" +
            "        \"pressure\": 1000.08,\n" +
            "        \"sea_level\": 1000.08,\n" +
            "        \"grnd_level\": 979.8,\n" +
            "        \"humidity\": 88,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.46,\n" +
            "        \"deg\": 240.5\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.004999999999999\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-12 06:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552381200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -3.74,\n" +
            "        \"temp_min\": -3.74,\n" +
            "        \"temp_max\": -3.74,\n" +
            "        \"pressure\": 1001.2,\n" +
            "        \"sea_level\": 1001.2,\n" +
            "        \"grnd_level\": 981.08,\n" +
            "        \"humidity\": 91,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 801,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"few clouds\",\n" +
            "          \"icon\": \"02d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 12\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.26,\n" +
            "        \"deg\": 242.004\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-12 09:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552392000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -3,\n" +
            "        \"temp_min\": -3,\n" +
            "        \"temp_max\": -3,\n" +
            "        \"pressure\": 1001.62,\n" +
            "        \"sea_level\": 1001.62,\n" +
            "        \"grnd_level\": 981.54,\n" +
            "        \"humidity\": 90,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 803,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"broken clouds\",\n" +
            "          \"icon\": \"04d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 64\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.36,\n" +
            "        \"deg\": 239.001\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-12 12:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552402800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -3.44,\n" +
            "        \"temp_min\": -3.44,\n" +
            "        \"temp_max\": -3.44,\n" +
            "        \"pressure\": 1002.61,\n" +
            "        \"sea_level\": 1002.61,\n" +
            "        \"grnd_level\": 982.34,\n" +
            "        \"humidity\": 89,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 76\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 4.72,\n" +
            "        \"deg\": 238.002\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.185\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-12 15:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1552413600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": -4.47,\n" +
            "        \"temp_min\": -4.47,\n" +
            "        \"temp_max\": -4.47,\n" +
            "        \"pressure\": 1004.33,\n" +
            "        \"sea_level\": 1004.33,\n" +
            "        \"grnd_level\": 983.95,\n" +
            "        \"humidity\": 83,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 600,\n" +
            "          \"main\": \"Snow\",\n" +
            "          \"description\": \"light snow\",\n" +
            "          \"icon\": \"13n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 20\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 3.7,\n" +
            "        \"deg\": 257.003\n" +
            "      },\n" +
            "      \"rain\": {},\n" +
            "      \"snow\": {\n" +
            "        \"3h\": 0.175\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-12 18:00:00\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"city\": {\n" +
            "    \"id\": 524901,\n" +
            "    \"name\": \"Moscow\",\n" +
            "    \"coord\": {\n" +
            "      \"lat\": 55.7507,\n" +
            "      \"lon\": 37.6177\n" +
            "    },\n" +
            "    \"country\": \"RU\",\n" +
            "    \"population\": 1000000\n" +
            "  }\n" +
            "}";

    @Test
    public void convertTest() throws Exception {
        WeatherResponse weatherResponse = OBJECT_MAPPER.readValue(W_R_DATA, WeatherResponse.class);
        System.out.println(weatherResponse);
    }

}