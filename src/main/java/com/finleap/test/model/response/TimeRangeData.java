package com.finleap.test.model.response;

import com.fasterxml.jackson.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeRangeData {
    @JsonIgnore
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private LocalDateTime localDateTime;
    @JsonProperty("main")
    private MainData mainData;

    public TimeRangeData() {
    }

    @JsonGetter("dt_txt")
    public String getLocalDateStr() {
        return localDateTime.format(FORMATTER);
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public TimeRangeData setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
        return this;
    }

    @JsonSetter("dt_txt")
    public TimeRangeData setLocalDateStr(String localDate) {
        this.localDateTime = LocalDateTime.parse(localDate, FORMATTER);
        return this;
    }

    public MainData getMainData() {
        return mainData;
    }

    public TimeRangeData setMainData(MainData mainData) {
        this.mainData = mainData;
        return this;
    }

    @Override
    public String toString() {
        return "TimeRangeData{" +
                "localDateTime=" + localDateTime +
                ", mainData=" + mainData +
                '}';
    }
}
