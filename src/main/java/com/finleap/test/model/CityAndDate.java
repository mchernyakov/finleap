package com.finleap.test.model;

import java.util.Objects;

public class CityAndDate {
    private final String city;
    // ISO date
    private final String date;

    public CityAndDate(String city, String date) {
        this.city = city;
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public String getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CityAndDate)) return false;
        CityAndDate that = (CityAndDate) o;
        return Objects.equals(city, that.city) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, date);
    }

    @Override
    public String toString() {
        return "CityAndDate{" +
                "city='" + city + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
