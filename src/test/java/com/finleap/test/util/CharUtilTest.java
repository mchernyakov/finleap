package com.finleap.test.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class CharUtilTest {

    @Test
    public void checkLettersInString() {
        assertTrue(CharUtil.checkLettersInString("Moscow"));
        assertTrue(CharUtil.checkLettersInString("Berlin"));
        assertTrue(CharUtil.checkLettersInString("Winston-Salem"));
    }

    @Test
    public void checkLettersInString0() {
        assertFalse(CharUtil.checkLettersInString("77Moscow77"));
        assertFalse(CharUtil.checkLettersInString("B0e0r0l0i0n0"));

        assertFalse(CharUtil.checkLettersInString("@M!o|s/c[o}w$"));
        assertFalse(CharUtil.checkLettersInString("B(e)r*l#i^n?"));
        assertFalse(CharUtil.checkLettersInString("Winston+Salem"));
    }
}