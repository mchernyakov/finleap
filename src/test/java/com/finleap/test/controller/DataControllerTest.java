package com.finleap.test.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finleap.test.model.Data;
import com.finleap.test.model.Range;
import com.finleap.test.service.DataService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class DataControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DataService dataService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.when(dataService.getWeatherDataByCity("Berlin")).thenReturn(new Data());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getById() throws Exception {
        Mockito.when(dataService.getWeatherDataByCity("Berlin"))
                .thenReturn(new Data()
                        .setAvgNight(-1.0)
                        .setAvgDay(2.0)
                        .setAvgPressure(700.0)
                        .setRange(new Range().setStartDate("2019-03-09").setEndDate("2019-03-11")));

        String path = "/data?city=Berlin";
        String respRes = this.mockMvc
                .perform(MockMvcRequestBuilders.get(path))
                .andDo(print())
                .andDo(document("get data"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(respRes);
    }

    @Test
    public void getById400() throws Exception {
        String path = "/data?city=B$erlin#";
        this.mockMvc
                .perform(MockMvcRequestBuilders.get(path))
                .andDo(print())
                .andDo(document("bad request"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
    }

    @Test
    public void getByIdAnother400() throws Exception {
        String path = "/data?q=Berlin";
        this.mockMvc
                .perform(MockMvcRequestBuilders.get(path))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
    }
}