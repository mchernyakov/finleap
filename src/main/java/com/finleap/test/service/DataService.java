package com.finleap.test.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finleap.test.model.CityAndDate;
import com.finleap.test.model.Data;
import com.finleap.test.model.Range;
import com.finleap.test.model.response.TimeRangeData;
import com.finleap.test.model.response.WeatherResponse;
import com.finleap.test.util.DateTimeUtil;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.typesafe.config.Config;
import org.apache.commons.math3.util.Precision;
import org.apache.http.HttpEntity;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class DataService {

    private final static Logger logger = LoggerFactory.getLogger(DataService.class);

    private final LoadingCache<CityAndDate, Data> cache;
    private final ObjectMapper mapper;
    private final String openWeatherKey;

    @Autowired
    public DataService(@Qualifier("service") Config serviceConfig, @Qualifier("app") ObjectMapper mapper) {
        this.mapper = mapper;
        this.openWeatherKey = serviceConfig.getString("key");
        Preconditions.checkArgument(!openWeatherKey.isEmpty());

        int maxCapacity = serviceConfig.getConfig("cache").getInt("max-capacity");
        int expireTimeSec = serviceConfig.getConfig("cache").getInt("expire-time-sec");
        Preconditions.checkArgument(maxCapacity > 0);
        Preconditions.checkArgument(expireTimeSec > 0);

        this.cache = Caffeine.newBuilder()
                .maximumSize(maxCapacity)
                .expireAfterWrite(expireTimeSec, TimeUnit.SECONDS)
                .build(this::getData);
    }

    @Nullable
    private Data getData(CityAndDate cityAndDate) {
        try {
            URIBuilder builder = new URIBuilder("https://api.openweathermap.org/data/2.5/forecast")
                    .setParameter("cnt", "32")
                    .setParameter("APPID", openWeatherKey)
                    .setParameter("units", "metric")
                    .setParameter("q", cityAndDate.getCity());

            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpGet httpGet = new HttpGet(builder.build());
                return httpClient.execute(httpGet, responseHandler());
            }
        } catch (Exception e) {
            logger.error("Error occurred while executing request for city {}", cityAndDate, e);
            throw new RuntimeException(e);
        }
    }

    private ResponseHandler<Data> responseHandler() {
        return httpResponse -> {
            int code = httpResponse.getStatusLine().getStatusCode();
            if (code != HttpStatus.OK.value()) {
                return null;
            }

            HttpEntity entity = httpResponse.getEntity();
            String body = EntityUtils.toString(entity);
            WeatherResponse weatherResponse = mapper.readValue(body, WeatherResponse.class);
            return buildData(weatherResponse);
        };
    }

    @VisibleForTesting
    @Nonnull
    Data buildData(@Nonnull WeatherResponse weatherResponse) {
        LocalDate now = LocalDate.now();
        List<TimeRangeData> afterFilter = weatherResponse.getList()
                .stream()
                .filter(val -> DateTimeUtil.checkDate(val, now))
                .collect(Collectors.toList());

        int size = afterFilter.size();

        final double[] avgPressureTmp = {0.0};
        final double[] avgNightTmp = {0.0};
        final double[] avgDayTmp = {0.0};
        final int[] counterD = {0};
        final int[] counterN = {0};

        afterFilter.forEach(timeRangeData -> {
            double p = timeRangeData.getMainData().getPressure();
            avgPressureTmp[0] += p;
            int h = timeRangeData.getLocalDateTime().getHour();
            if (h >= 18 || h < 6) {
                avgNightTmp[0] += timeRangeData.getMainData().getTemp();
                counterN[0]++;
            } else {
                avgDayTmp[0] += timeRangeData.getMainData().getTemp();
                counterD[0]++;
            }
        });

        Range range = new Range()
                .setStartDate(DateTimeUtil.getNextDay())
                .setEndDate(DateTimeUtil.getEndDay());
        return new Data().setRange(range)
                .setAvgPressure(buildValue(avgPressureTmp[0], size))
                .setAvgDay(buildValue(avgDayTmp[0], counterD[0]))
                .setAvgNight(buildValue(avgNightTmp[0], counterN[0]));
    }

    private static double buildValue(double val, int delimiter) {
        return Precision.round(val / delimiter, 3);
    }

    @Nonnull
    public Data getWeatherDataByCity(@Nonnull String city) {
        CityAndDate cityAndDate = buildCacheKey(city);
        Data res = cache.get(cityAndDate);
        if (res == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else return res;
    }

    @Nonnull
    private static CityAndDate buildCacheKey(@Nonnull String city) {
        String date = DateTimeUtil.getCurrentDate();
        return new CityAndDate(city, date);
    }
}
