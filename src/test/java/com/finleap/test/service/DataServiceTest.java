package com.finleap.test.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finleap.test.model.Data;
import com.finleap.test.model.response.WeatherResponse;
import com.finleap.test.model.response.WeatherResponseTest;
import com.finleap.test.util.DateTimeUtil;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.math3.util.Precision;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class DataServiceTest {

    private DataService dataService;

    @Before
    public void setUp() throws Exception {
        Config config = ConfigFactory.load();
        dataService = new DataService(config.getConfig("service"), new ObjectMapper());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = ResponseStatusException.class)
    public void getWeatherDataByCity0() {
        dataService.getWeatherDataByCity("Not-A-City");
    }

    @Test
    public void resCalcTest() throws Exception {
        WeatherResponse weatherResponse = new ObjectMapper().readValue(WeatherResponseTest.W_R_DATA, WeatherResponse.class);
        Data data = dataService.buildData(weatherResponse);
        System.out.println(data);

        double avgP = Precision.round(weatherResponse.getList().stream()
                        .filter(v -> DateTimeUtil.checkDate(v, LocalDate.now()))
                        .mapToDouble(value -> value.getMainData().getPressure())
                        .average()
                        .orElse(Double.NaN),
                3);

        double avgD = Precision.round(weatherResponse.getList().stream()
                        .filter(v -> DateTimeUtil.checkDate(v, LocalDate.now()))
                        .filter(val -> !(val.getLocalDateTime().getHour() >= 18 || val.getLocalDateTime().getHour() < 6))
                        .mapToDouble(value -> value.getMainData().getTemp())
                        .average()
                        .orElse(Double.NaN),
                3);

        double avgN = Precision.round(weatherResponse.getList().stream()
                        .filter(v -> DateTimeUtil.checkDate(v, LocalDate.now()))
                        .filter(val -> val.getLocalDateTime().getHour() >= 18 || val.getLocalDateTime().getHour() < 6)
                        .mapToDouble(value -> value.getMainData().getTemp())
                        .average()
                        .orElse(Double.NaN),
                3);

        assertEquals(avgP, data.getAvgPressure(), 0.001);
        assertEquals(avgD, data.getAvgDay(), 0.001);
        assertEquals(avgN, data.getAvgNight(), 0.001);
    }
}