package com.finleap.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Data {

    @JsonProperty("range")
    private Range range;
    @JsonProperty("avg_night")
    private Double avgNight;
    @JsonProperty("avg_day")
    private Double avgDay;
    @JsonProperty("avg_pressure")
    private Double avgPressure = 0.0;

    public Range getRange() {
        return range;
    }

    public Data setRange(Range range) {
        this.range = range;
        return this;
    }

    public Double getAvgNight() {
        return avgNight;
    }

    public Data setAvgNight(Double avgNight) {
        this.avgNight = avgNight;
        return this;
    }

    public Double getAvgDay() {
        return avgDay;
    }

    public Data setAvgDay(Double avgDay) {
        this.avgDay = avgDay;
        return this;
    }

    public Double getAvgPressure() {
        return avgPressure;
    }

    public Data setAvgPressure(Double avgPressure) {
        this.avgPressure = avgPressure;
        return this;
    }

    @Override
    public String toString() {
        return "Data{" +
                "range=" + range +
                ", avgNight=" + avgNight +
                ", avgDay=" + avgDay +
                ", avgPressure=" + avgPressure +
                '}';
    }
}
