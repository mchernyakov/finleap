package com.finleap.test.util;

import com.finleap.test.model.response.TimeRangeData;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DateTimeUtilTest {

    @Test
    public void getCurrentDate() {
        System.out.println(DateTimeUtil.getCurrentDate());
    }

    @Test
    public void getNextDay() {
        System.out.println(DateTimeUtil.getNextDay());
    }

    @Test
    public void getEndDay() {
        System.out.println(DateTimeUtil.getEndDay());
    }

    @Test
    public void testCheck0() throws Exception {
        LocalDate localDateTime = LocalDate.parse("2019-03-08 18:01:01", TimeRangeData.FORMATTER);
        TimeRangeData timeRangeData = new TimeRangeData().setLocalDateTime(LocalDateTime.parse("2019-03-08 18:02:01", TimeRangeData.FORMATTER));
        assertFalse(DateTimeUtil.checkDate(timeRangeData, localDateTime));
    }

    @Test
    public void testCheck1() throws Exception {
        LocalDate localDateTime = LocalDate.parse("2019-03-08 18:01:01", TimeRangeData.FORMATTER);
        TimeRangeData timeRangeData = new TimeRangeData().setLocalDateTime(LocalDateTime.parse("2019-03-09 00:00:01", TimeRangeData.FORMATTER));
        assertTrue(DateTimeUtil.checkDate(timeRangeData, localDateTime));
    }

    @Test
    public void testCheck2() throws Exception {
        LocalDate localDateTime = LocalDate.parse("2019-03-08 18:01:01", TimeRangeData.FORMATTER);
        TimeRangeData timeRangeData = new TimeRangeData().setLocalDateTime(LocalDateTime.parse("2019-03-12 00:00:01", TimeRangeData.FORMATTER));
        assertFalse(DateTimeUtil.checkDate(timeRangeData, localDateTime));
    }

    @Test
    public void testCheck3() throws Exception {
        LocalDate localDateTime = LocalDate.parse("2019-03-08 18:01:01", TimeRangeData.FORMATTER);
        TimeRangeData timeRangeData = new TimeRangeData().setLocalDateTime(LocalDateTime.parse("2019-03-11 23:59:59", TimeRangeData.FORMATTER));
        assertTrue(DateTimeUtil.checkDate(timeRangeData, localDateTime));
    }
}