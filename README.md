# Description
## Project
Project tree:
 ```text
├── README.md
├── finleap.iml // ignore
├── pom.xml
├── src
└── target // ignore
```

#### Requirements
* Java 8
* Maven 3.3.*
* Mac OS or Linux (I have mac)

## Idea
Simple idea: web-server based on SpringBoot which retrieve data from openweather API.

I've decided to add cache inside the application for better performance.
Key in the cache is a combination of city and a current date.

You can set expire time for value in the cache in configuration.

## Technologies

#### Spring Boot
Well-knowing framework for microservices and other web-application. 

#### TypesafeConfig
[Typesafe Config (Lightbend)](https://github.com/lightbend/config) -- 
my favorite library for configuration. It is very flexible tool, so, I've decided to use it.

#### Caffeine
[Caffeine](https://github.com/ben-manes/caffeine) 
Well-known framework for caching inside your application.

#### Apache Http client
Simple and useful HTTP-client.

## How to use

#### Build

Build jar and run tests:

```bash
mvn clean package 
```
or
```bash
mvn clean package -Dmaven.test.skip=true
```
#### How to run

Run:
```bash
java -jar target/finleap-test-1.0.0.jar

```
Run with custom **reference.conf** (this is file with properties for service), 
you have to set path to file ():
```bash
java -jar -Dconfig.file=<path to file>/reference.conf target/finleap-test-1.0.0.jar

```
Example:
 ```bash
java -jar -Dconfig.file=build/etc/reference.conf target/finleap-test-1.0.0.jar

 ```
or, for example, you can set a parameter using **-D**:
java -jar -Dserver.port=8080 target/finleap-test-1.0.0.jar

#### Parameters
Service has some parameters (_reference.conf_) :
 ```hocon
server { 
  spring {
    profiles = [prod] # profiles
  }
}

service {
  key = "d42250bd1666d2841e529142dc7f27da" # open weather key

  cache { # cache settings
    max-capacity = 100
    expire-time-sec = 3600
  }
}

```

#### Requests

Request has to have parameter **city**.

##### GET 200

Request :

  ```text
$ curl 'http://localhost:8080/data?city=Berlin' -i -X GET
```

Response would be something like this:

 ```text
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Content-Length: 113

{
  "range": {
    "start_date": "2019-03-09",
    "end_date": "2019-03-11"
  },
  "avg_night": -1,
  "avg_day": 2,
  "avg_pressure": 700
}

```
##### GET 400

Request :

```text
$ curl 'http://localhost:8080/data?city=B$erlin' -i -X GET
```

Response:

```text
 HTTP/1.1 400 Bad Request
```