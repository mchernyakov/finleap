package com.finleap.test.util;

import com.finleap.test.model.response.TimeRangeData;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class DateTimeUtil {

    private DateTimeUtil() {
    }

    public static String getCurrentDate() {
        LocalDate localDate = LocalDate.now();
        return localDate.format(DateTimeFormatter.ISO_DATE);
    }

    public static String getNextDay() {
        LocalDate localDate = LocalDate.now().plusDays(1);
        return localDate.format(DateTimeFormatter.ISO_DATE);
    }

    public static String getEndDay() {
        LocalDate localDate = LocalDate.now().plusDays(3);
        return localDate.format(DateTimeFormatter.ISO_DATE);
    }

    public static boolean checkDate(@Nonnull TimeRangeData input, LocalDate nowDate) {
        LocalDate inputDate = input.getLocalDateTime().toLocalDate();
        LocalDate endDate = nowDate.plusDays(3);
        return nowDate.isBefore(inputDate) && (endDate.isEqual(inputDate) || endDate.isAfter(inputDate));
    }
}
