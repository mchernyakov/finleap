package com.finleap.test.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MainData {
    private Float temp;
    private Float pressure;

    public MainData() {
    }

    public Float getTemp() {
        return temp;
    }

    public MainData setTemp(Float temp) {
        this.temp = temp;
        return this;
    }

    public Float getPressure() {
        return pressure;
    }

    public MainData setPressure(Float pressure) {
        this.pressure = pressure;
        return this;
    }

    @Override
    public String toString() {
        return "MainData{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                '}';
    }
}
