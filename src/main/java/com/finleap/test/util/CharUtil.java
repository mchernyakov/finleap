package com.finleap.test.util;

public final class CharUtil {

    private CharUtil() {
    }

    public static boolean checkLettersInString(String s) {
        return s.matches("[a-zA-Z]+(?:[ '-][a-zA-Z]+)*");
    }
}
