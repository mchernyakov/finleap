package com.finleap.test.model.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class TimeRangeDataTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final String DATA = "    {\n" +
            "      \"dt\": 1552068000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 2.36,\n" +
            "        \"temp_min\": 2.36,\n" +
            "        \"temp_max\": 2.62,\n" +
            "        \"pressure\": 1005.36,\n" +
            "        \"sea_level\": 1005.36,\n" +
            "        \"grnd_level\": 985.5,\n" +
            "        \"humidity\": 92,\n" +
            "        \"temp_kf\": -0.26\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 500,\n" +
            "          \"main\": \"Rain\",\n" +
            "          \"description\": \"light rain\",\n" +
            "          \"icon\": \"10n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 92\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 5.02,\n" +
            "        \"deg\": 208.5\n" +
            "      },\n" +
            "      \"rain\": {\n" +
            "        \"3h\": 0.915\n" +
            "      },\n" +
            "      \"snow\": {},\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2019-03-08 18:00:00\"\n" +
            "    }";

    @Test
    public void convertTest() throws Exception {
        TimeRangeData timeRangeData = OBJECT_MAPPER.readValue(DATA, TimeRangeData.class);
        System.out.println(timeRangeData);
        assertNotNull(timeRangeData.getMainData());
        assertEquals("2019-03-08 18:00:00",timeRangeData.getLocalDateStr());
    }

}