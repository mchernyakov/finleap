package com.finleap.test.model.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainDataTest {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final String DATA = "{\n" +
            "        \"temp\": 2.36,\n" +
            "        \"temp_min\": 2.36,\n" +
            "        \"temp_max\": 2.62,\n" +
            "        \"pressure\": 1005.36,\n" +
            "        \"sea_level\": 1005.36,\n" +
            "        \"grnd_level\": 985.5,\n" +
            "        \"humidity\": 92,\n" +
            "        \"temp_kf\": -0.26\n" +
            "      }";

    @Test
    public void convertTest() throws Exception{
        MainData mainData = MAPPER.readValue(DATA,MainData.class);
        System.out.println(mainData);
        assertEquals(2.36, mainData.getTemp(),0.01);
        assertEquals(1005.36, mainData.getPressure(),0.01);
    }

}