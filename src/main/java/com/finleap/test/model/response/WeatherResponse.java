package com.finleap.test.model.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {

    @JsonProperty("list")
    private List<TimeRangeData> list;

    @JsonCreator
    public WeatherResponse(@JsonProperty("list") List<TimeRangeData> list) {
        this.list = list;
    }

    public List<TimeRangeData> getList() {
        return list;
    }

    public WeatherResponse setList(List<TimeRangeData> list) {
        this.list = list;
        return this;
    }

    @Override
    public String toString() {
        return "WeatherResponse{" +
                "list=" + list +
                '}';
    }
}
